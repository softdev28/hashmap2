
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACER
 */
public class Hashmap {

    int Map;
    ArrayList<String> table = new ArrayList<String>();

    public Hashmap(int Map) {
        this.Map = Map;
        for (int i = 0; i < Map; i++) {
            table.add(i, null);
        }

    }

    private int hashFunction(int k) {
        int h = k % Map;
        return h;
    }

    public String get(int k) {
        int h = hashFunction(k);
        return table.get(h);
    }

    public void getAll() {
        for (int i = 0; i < Map; i++) {
            System.out.println("[ Key : " + i + " , Value : " + table.get(i) + " ]");
        }
    }

    public void put(int k, String v) {
        int h = hashFunction(k);
        table.set(h, v);
    }

    public void remove(int k) {
        int h = hashFunction(k);
        table.set(h, null);
    }
}
